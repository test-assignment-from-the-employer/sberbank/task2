Есть некий сервис с API по адрессу; http://someurl.ru/api/ 
POST/PUT - принимает json файл с данными
DELETE - удаляет файл id
GET - получает файл по id
формат json файла:
{
id: '12345'
info1: 'someinfo1'
info2: 'someinfo2'
}
Написать пайплайн который будет формировать данный файл и отправлять и получать его на сервис, проверяя при этом статус кода от сервиса (200;403;503)
параметры Jenkins Job:
id,info2,info2 - STRING
mode - Choise; значения GET/SEND/DELETE